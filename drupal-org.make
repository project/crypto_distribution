api = 2
core = 8.x

; Modules

projects[addtoany][version] = 1.14
projects[addtoany][subdir] = contrib

projects[contact_block][version] = 1.5
projects[contact_block][subdir] = contrib

projects[ctools][version] = 3.4
projects[ctools][subdir] = contrib

projects[default_content][version] = 1.0-alpha9
projects[default_content][subdir] = contrib

projects[feeds][version] = 3.0-alpha9
projects[feeds][subdir] = contrib

projects[field_group][version] = 3.1
projects[field_group][subdir] = contrib

projects[fontawesome][version] = 2.17
projects[fontawesome][subdir] = contrib

projects[pathauto][version] = 1.8
projects[pathauto][subdir] = contrib

projects[superfish][version] = 1.4
projects[superfish][subdir] = contrib

projects[token][version] = 1.9
projects[token][subdir] = contrib

projects[twig_tweak][version] = 2.9
projects[twig_tweak][subdir] = contrib

projects[views_arg_order_sort][version] = 1.x-dev
projects[views_arg_order_sort][subdir] = contrib
projects[views_arg_order_sort][patch][] = "https://www.drupal.org/files/issues/2021-01-30/views_arg_order_sort_d9_0.patch"

; Themes

projects[bootstrap][version] = 3.23

; Libraries
libraries[superfish][directory_name] = superfish
libraries[superfish][type] = library
libraries[superfish][destination] = libraries
libraries[superfish][download][type] = get
libraries[superfish][download][url] = https://github.com/mehrpadin/Superfish-for-Drupal/archive/2.x.zip
